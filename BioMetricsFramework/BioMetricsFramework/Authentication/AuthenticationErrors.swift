//
//  AuthenticationErrors.swift
//  BioMetricsFramework
//
//  Created by Nick Watson on 21/12/2017.
//  Copyright © 2017 Nick Watson. All rights reserved.
//

import Foundation
import LocalAuthentication

/// Authentication Errors
public enum AuthenticationError {
    
    case failed, canceledByUser, fallback, canceledBySystem, passcodeNotSet, biometryNotEnrolled, biometryLockedout, other
    
    public static func `init`(error: LAError) -> AuthenticationError {
        switch Int32(error.errorCode) {
            
        case kLAErrorAuthenticationFailed:
            return failed
        case kLAErrorUserCancel:
            return canceledByUser
        case kLAErrorUserFallback:
            return fallback
        case kLAErrorSystemCancel:
            return canceledBySystem
        case kLAErrorPasscodeNotSet:
            return passcodeNotSet
        case kLAErrorBiometryNotEnrolled:
            return biometryNotEnrolled
        case kLAErrorBiometryLockout:
            return biometryLockedout
        default:
            return other
        }
    }
    
    // get error message based on type
    public func message() -> String {
        let authentication = BioMetricsAuthenticator.shared
        
        switch self {
        case .canceledByUser, .fallback, .canceledBySystem:
            return ""
        case .passcodeNotSet:
            return authentication.faceIDAvailable() ? cSetPasscodeToUseFaceID : cSetPasscodeToUseTouchID
        case .biometryNotEnrolled:
            return authentication.faceIDAvailable() ? cNoFaceIdentityEnrolled : cNoFingerprintEnrolled
        case .biometryLockedout:
            return authentication.faceIDAvailable() ? cFaceIdPasscodeAuthenticationReason : cTouchIdPasscodeAuthenticationReason
        default:
            return authentication.faceIDAvailable() ? cDefaultFaceIDAuthenticationFailedReason : cDefaultTouchIDAuthenticationFailedReason
        }
    }
}

