//
//  BioMetricsAuthenticator.swift
//  BioMetricsFramework
//
//  Created by Nick Watson on 21/12/2017.
//  Copyright © 2017 Nick Watson. All rights reserved.
//

import UIKit
import LocalAuthentication

open class BioMetricsAuthenticator: NSObject {
    
    // MARK: - Singleton
    public static let shared = BioMetricsAuthenticator()
}

// MARK:- Public

public extension BioMetricsAuthenticator {
    
    /// checks if TouchID or FaceID is available on the device.
    class func canAuthenticate() -> Bool {
        
        var isBiometricsAuthenticationAvailable = false
        var error: NSError? = nil
        
        if LAContext().canEvaluatePolicy(LAPolicy.deviceOwnerAuthenticationWithBiometrics, error: &error) {
            isBiometricsAuthenticationAvailable = (error == nil)
        }
        return isBiometricsAuthenticationAvailable
    }
    
    /// Check for biometric authentication
    class func authenticateWithBioMetrics(reason: String, fallbackTitle: String? = "", cancelTitle: String? = "", success successBlock:@escaping AuthenticationSuccess, failure failureBlock:@escaping AuthenticationFailure) {
        let reasonString = reason.isEmpty ? BioMetricsAuthenticator.shared.defaultBiometricsAuthenticationReason() : reason
        
        let context = LAContext()
        context.localizedFallbackTitle = fallbackTitle
        
        // cancel button title
        if #available(iOS 10.0, *) {
            context.localizedCancelTitle = cancelTitle
        }
        
        // authenticate
        BioMetricsAuthenticator.shared.evaluate(policy: LAPolicy.deviceOwnerAuthenticationWithBiometrics, with: context, reason: reasonString, success: successBlock, failure: failureBlock)
    }
    
    /// Check for device passcode authentication
    class func authenticateWithPasscode(reason: String, cancelTitle: String? = "", success successBlock:@escaping AuthenticationSuccess, failure failureBlock:@escaping AuthenticationFailure) {
        let reasonString = reason.isEmpty ? BioMetricsAuthenticator.shared.defaultPasscodeAuthenticationReason() : reason
        
        let context = LAContext()
        
        // cancel button title
        if #available(iOS 10.0, *) {
            context.localizedCancelTitle = cancelTitle
        }
        
        // authenticate
        BioMetricsAuthenticator.shared.evaluate(policy: LAPolicy.deviceOwnerAuthentication, with: context, reason: reasonString, success: successBlock, failure: failureBlock)
    }
    
    /// checks if face id is avaiable on device
    public func faceIDAvailable() -> Bool {
        if #available(iOS 11.0, *) {
            let context = LAContext()
            return (context.canEvaluatePolicy(LAPolicy.deviceOwnerAuthenticationWithBiometrics, error: nil) && context.biometryType == .faceID)
        }
        return false
    }
}

// MARK:- Private
extension BioMetricsAuthenticator {
    
    /// get authentication reason to show while authentication
    func defaultBiometricsAuthenticationReason() -> String {
        return faceIDAvailable() ? cFaceIdAuthenticationReason : cTouchIdAuthenticationReason
    }
    
    /// get passcode authentication reason to show while entering device passcode after multiple failed attempts.
    func defaultPasscodeAuthenticationReason() -> String {
        return faceIDAvailable() ? cFaceIdPasscodeAuthenticationReason : cTouchIdPasscodeAuthenticationReason
    }
    
    /// evaluate policy
    func evaluate(policy: LAPolicy, with context: LAContext, reason: String, success successBlock:@escaping AuthenticationSuccess, failure failureBlock:@escaping AuthenticationFailure) {
        
        context.evaluatePolicy(policy, localizedReason: reason) { (success, err) in
            DispatchQueue.main.async {
                if success { successBlock() }
                else {
                    let errorType = AuthenticationError(error: err as! LAError)
                    failureBlock(errorType)
                }
            }
        }
    }
}

