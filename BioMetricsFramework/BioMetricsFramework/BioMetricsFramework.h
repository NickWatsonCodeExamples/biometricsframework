//
//  BioMetricsFramework.h
//  BioMetricsFramework
//
//  Created by Nick Watson on 21/12/2017.
//  Copyright © 2017 Nick Watson. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for BioMetricsFramework.
FOUNDATION_EXPORT double BioMetricsFrameworkVersionNumber;

//! Project version string for BioMetricsFramework.
FOUNDATION_EXPORT const unsigned char BioMetricsFrameworkVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <BioMetricsFramework/PublicHeader.h>


