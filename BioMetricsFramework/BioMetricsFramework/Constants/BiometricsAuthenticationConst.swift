//
//  BiometricsAuthenticationConst.swift
//  BioMetricsFramework
//
//  Created by Nick Watson on 21/12/2017.
//  Copyright © 2017 Nick Watson. All rights reserved.
//

import Foundation
import LocalAuthentication

/// success block
public typealias AuthenticationSuccess = (() -> ())

/// failure block
public typealias AuthenticationFailure = ((AuthenticationError) -> ())


/// ****************  Touch ID  ****************** ///

let cTouchIdAuthenticationReason = "Confirm your fingerprint to authenticate."
let cTouchIdPasscodeAuthenticationReason = "Touch ID is locked now, because of too many failed attempts. Enter passcode to unlock Touch ID."

/// Error Messages Touch ID
let cSetPasscodeToUseTouchID = "Please set device passcode to use Touch ID for authentication."
let cNoFingerprintEnrolled = "There are no fingerprints enrolled in the device. Please go to Device Settings -> Touch ID and Passcode and enroll your fingerprints."
let cDefaultTouchIDAuthenticationFailedReason = "Touch ID does not recognize your fingerprint. Please try again with your enrolled fingerprint."

/// ****************  Face ID  ****************** ///

let cFaceIdAuthenticationReason = "Confirm your face to authenticate."
let cFaceIdPasscodeAuthenticationReason = "Face ID is locked now, because of too many failed attempts. Enter passcode to unlock Face ID."

/// Error Messages Face ID
let cSetPasscodeToUseFaceID = "Please set device passcode to use Face ID for authentication."
let cNoFaceIdentityEnrolled = "There are no face enrolled in the device. Please go to Device Settings -> Face ID and Passcode and enroll your face."
let cDefaultFaceIDAuthenticationFailedReason = "Face ID does not recognize your face. Please try again with your enrolled face."

